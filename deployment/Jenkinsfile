def label = "cicd-sample-${env.BUILD_NUMBER}"

/* -------- functions ---------- */
def notifySlack(STATUS, COLOR) {
	slackSend (channel: '#cicd-sample', color: COLOR, message: STATUS+" : " +  "${env.JOB_NAME} [${env.BUILD_NUMBER}] (${env.BUILD_URL})")
}

notifySlack("STARTED", "#FFFF00")

podTemplate(
	label: label, 
	containers: [
		//container image는 docker search 명령 이용
		containerTemplate(name: "trivy", image: "aquasec/trivy", ttyEnabled: true, command: "cat"),
		containerTemplate(name: "kubectl", image: "lachlanevenson/k8s-kubectl", command: "cat", ttyEnabled: true),
		containerTemplate(name: "scanner", image: "newtmitch/sonar-scanner", ttyEnabled: true, command: "cat"),
		//containerTemplate(name: 'podman', image: 'ibmcase/podman:ubuntu-16.04', ttyEnabled: true, command: 'cat', privileged: true)
		containerTemplate(name: 'podman', image: 'mgoltzsche/podman', ttyEnabled: true, command: 'cat', privileged: true)
	],
	//volume mount
	volumes: [
		//hostPathVolume(hostPath: "/var/run/docker.sock", mountPath: "/var/run/docker.sock")
	]
) 
{
	node(label) {
		stage("Get Source") {
			git branch: 'master', url: 'https://gitlab.com/msa2021/cicd-nodejs-sample.git'
        } 

		//-- 환경변수 파일 읽어서 변수값 셋팅 
		def props = readProperties  file:"deployment/pipeline.properties"
		def tag = props["version"]
		def dockerRegistry = props["dockerRegistry"]
		def credentialRegistry=props["credentialRegistry"]
		def image = props["image"]
		def deployment = props["deployment"]
		def service = props["service"]
		def ingress = props["ingress"]
		def selector_key = props["selector_key"]
		def selector_val = props["selector_val"]
		def namespace = props["namespace"] 
		def app_url = ""

		// tag를 재정의 함. 이미지 버전이 달라야 배포시 컨테이너에서 인식
		// def timeStamp = System.currentTimeMillis()
		// echo "TimeStamp: ${timeStamp}"
		// echo "Tag : ${tag}"
		// tag = tag+"-"+timeStamp
		// echo "New Tag : ${tag}"	


		try {
			
            stage("Inspection Code") {
				container("scanner") {
					sh "sonar-scanner \
						-Dsonar.projectName=user00-nodejs-sample \
					  	-Dsonar.projectKey=user00-nodejs-sample \
					  	-Dsonar.sources=. \
						-Dsonar.projectBaseDir=. \
						-Dsonar.language=javascript \
					  	-Dsonar.host.url=http://169.56.71.190:32535/sonar \
					  	-Dsonar.login=1f26d285ed1a74349557c47edc201ffaf2dede0c"

				}
			} 

			stage("Build Microservice image") {
				container("podman") {
					withCredentials([usernamePassword(
					                   credentialsId: "${credentialRegistry}",
					                   usernameVariable: 'USER',
					                   passwordVariable: 'PASSWORD'
					               )]) {
					    //sh 'echo user "$USER" pasword "$PASSWORD"'
					    //sh "podman login --tls-verify=false --username ${USER} --password ${PASSWORD} ${dockerRegistry}"
					    sh "podman login --tls-verify=false --username ${USER} --password ${PASSWORD}"
						sh "podman build -f ./deployment/Dockerfile -t ${image}:${tag} ."
						sh "sleep 2"
						sh "podman push ${image}:${tag}"
						sh "podman tag ${image}:${tag} ${image}:latest"
						sh "podman push ${image}:latest"
					}
				}
			}

			stage("Image Vulnerability Scanning") {
				container("trivy"){
					sh "trivy image ${image}:latest"
					
				}
			}

			stage( "Clean Up Existing Deployments" ) {
				container("kubectl") {
					sh "kubectl delete deployments -n ${namespace} --selector=${selector_key}=${selector_val}"
				}
			}

			stage( "Deploy to Cluster" ) {
				container("kubectl") {
					sh "kubectl apply -n ${namespace} -f ${deployment}"
					sh "sleep 5"
					sh "kubectl apply -n ${namespace} -f ${service}"
					sh "kubectl apply -n ${namespace} -f ${ingress}"
					//sh "kubectl patch ing hellonode -p '{\"spec\":{\"rules\":[{\"host\":\"${selector_val}-${namespace}.169.56.71.190.nip.io\"}]}}'"
					sh "kubectl get ingress -n ${namespace} --no-headers | awk \'{print \$3}\' > sh-result"
					app_url = readFile('sh-result').trim()
				}
			}

            notifySlack("${currentBuild.currentResult}", "#00FF00")
            
			echo "**** FINISH ALL STAGES : SUCCESS"
			echo " Home URL : http://${app_url}"


		} catch(e) {
			currentBuild.result = "FAILED"
            notifySlack("${currentBuild.currentResult}", "#FF0000")
            
		}
	}
}